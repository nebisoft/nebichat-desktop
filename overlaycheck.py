#!/usr/bin/env python3
import gi, os
gi.require_version('Gtk', '3.0')
gi.require_version('Wnck', '3.0')
from gi.repository import Gtk, Wnck
def state_changed(window, changed_mask, new_state):
    ActiveChanged(wnck_scr, window)

def ActiveChanged(screen, previous_window):
    try:
        window = screen.get_active_window()
        classname = window.get_class_group_name()
        geometry = window.get_geometry()
        isFullScreen = geometry.xp + geometry.yp + wnck_scr.get_width() + wnck_scr.get_height() == wnck_scr.get_width() + wnck_scr.get_height()
        window.connect("state_changed", state_changed)
        print(classname)
        if not classname.__contains__("steam_app_nebichat_overlay"):
            os.system("pkill -9 -f overlay.py")
        if isFullScreen and str(classname).startswith("steam_app_"):
            os.system('notify-send "NebiChat" "Press CTRL + Tab to activate chat overlay." &')
            print("Enable Overlay")
            os.system("python3 ./overlay.py -name steam_app_nebichat_overlay")
        else:
        	os.system("pkill -9 -f overlay.py")
    except Exception as e:
        print(e)


wnck_scr = Wnck.Screen.get_default()
wnck_scr.force_update()
wnck_scr.connect("active-window-changed", ActiveChanged)
Gtk.main()