import os
import sys
from PySide6.QtCore import *
from PySide6.QtGui import *
from PySide6.QtWebEngineCore import *
from PySide6.QtWebEngineWidgets import *
from pynput import keyboard
import multiprocessing
from PySide6.QtWidgets import *

class Overlay(QMainWindow):
	def on_finish(self, arg1):
		if self.refreshcount == 0:
			self.refreshcount = 1
			self.wm.reload()

	def closeEvent(self, event):
		exit(0)

	def __init__(self):
		super().__init__()
		self.refreshcount = 0
		self.setAttribute(Qt.WidgetAttribute.WA_TranslucentBackground, True)
		self.setAttribute(Qt.WA_X11NetWmWindowTypePopupMenu, True)
		exitOverlay = QShortcut(self)
		exitOverlay.setEnabled(True)
		exitOverlay.setKey(Qt.CTRL + Qt.Key.Key_Tab)
		exitOverlay.activated.connect(self.close)

		self.profile = QWebEngineProfile(f"Default")
		self.profile.setPersistentCookiesPolicy(QWebEngineProfile.PersistentCookiesPolicy.ForcePersistentCookies)
		self.profile.cookieStore().loadAllCookies()

		self.wm = QWebEngineView()

		webpage = QWebEnginePage(self.profile, self.wm)

		self.wm.setPage(webpage)

		self.wm.settings().setAttribute(QWebEngineSettings.LocalContentCanAccessRemoteUrls, True)
		self.wm.loadFinished.connect(self.on_finish)
		self.wm.page().setBackgroundColor(Qt.GlobalColor.transparent)
		self.wm.setAttribute(Qt.WA_TranslucentBackground, True)
		self.wm.setAttribute(Qt.WidgetAttribute.WA_OpaquePaintEvent, False)
		self.setAttribute(Qt.WA_NoSystemBackground)
		self.wm.setStyleSheet("background: transparent;")
		self.wm.setAutoFillBackground(True)
		print(self.wm.url())
		self.setCentralWidget(self.wm)
		self.setWindowFlags(
			Qt.WindowStaysOnTopHint | Qt.FramelessWindowHint)


def detectPress():
	global stopThreads

	def on_activate():
		print('Global hotkey activated!')
		app = QApplication(sys.argv)

		app.setApplicationName("NebiChat")
		overlay = Overlay()
		desktop = QApplication.primaryScreen()
		screen_rect = desktop.availableGeometry()
		overlay.resize(screen_rect.width(), screen_rect.height())
		print(screen_rect.width())
		overlay.showFullScreen()
		overlay.show()
		overlay.wm.load(QUrl("https://chat.nebisoftware.com"))
		overlay.wm.load(QUrl.fromLocalFile(os.getcwd() + "/ui/overlay/index.html"))
		app.exec_()

	def for_canonical(f):
	    return lambda k: f(l.canonical(k))
	    
	hotkey = keyboard.HotKey(
	    keyboard.HotKey.parse('<ctrl>+<tab>'),
	    on_activate)
	with keyboard.Listener(
		on_press=for_canonical(hotkey.press)) as l:
	    l.join()

if __name__ == "__main__":
	t = multiprocessing.Process(target=detectPress)
	t.start()
