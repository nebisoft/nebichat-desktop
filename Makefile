.PHONY: install-dep build-nebios build-generic-linux

install-dep:
	sudo apt install -y patchelf
	sudo pip3 install cx_freeze pyside6==6.6.2

build-nebios:
	rm -rf ./build/exe.nebios/
	mkdir -p ./build/exe.nebios/unpacked
	cp ./overlay.py ./build/exe.nebios/unpacked
	cp ./overlaycheck.py ./build/exe.nebios/unpacked
	cp ./NebiChat.py ./build/exe.nebios/unpacked
	cp -r ./ui ./build/exe.nebios/unpacked
	cp -r ./lib ./build/exe.nebios/unpacked
	python3 -m compileall ./build/exe.nebios/unpacked
	python3 -m venv ./build/exe.nebios/unpacked/venv
	./build/exe.nebios/unpacked/venv/bin/pip install pyside6==6.6.2
	echo '#!/bin/bash' > ./build/exe.nebios/unpacked/app.exec
	echo 'export PATH=$$PWD/venv/bin:$$PATH' >> ./build/exe.nebios/unpacked/app.exec
	echo 'bash $$PWD/venv/bin/activate' >> ./build/exe.nebios/unpacked/app.exec
	echo 'python3 ./NebiChat.py $$@' >> ./build/exe.nebios/unpacked/app.exec
	chmod +x ./build/exe.nebios/unpacked/app.exec
	cp ./icon.png ./build/exe.nebios/NebiChat.png
	cp ./NebiChat.ninf ./build/exe.nebios/NebiChat.ninf
	exec2napp ./build/exe.nebios/unpacked/ ./build/exe.nebios/NebiChat.ninf ./build/exe.nebios/NebiChat.napp

PYTHON_VERSION := $(shell python3 -c "import sys; print(str(sys.version_info.major) + '.' + str(sys.version_info.minor))")
ARCHITECTURE := $(shell uname -m)
LINUX_BUILD_DIR := ./build/exe.linux-$(ARCHITECTURE)-$(PYTHON_VERSION)
APPIMAGE_TOOL := appimagetool-$(ARCHITECTURE).AppImage

build-generic-linux:
	rm -rf $(LINUX_BUILD_DIR)
	python3 ./tools/cx-freeze_setup.py build
	echo '#!/bin/bash' > $(LINUX_BUILD_DIR)/AppRun
	echo 'HERE="$$(dirname "$$(readlink -f "$${0}")")"' >> $(LINUX_BUILD_DIR)/AppRun
	echo 'cd $$HERE' >> $(LINUX_BUILD_DIR)/AppRun
	echo './NebiChat "$$@"' >> $(LINUX_BUILD_DIR)/AppRun
	chmod +x $(LINUX_BUILD_DIR)/AppRun
	echo '[Desktop Entry]' > $(LINUX_BUILD_DIR)/NebiChat.desktop
	echo 'Type=Application' >> $(LINUX_BUILD_DIR)/NebiChat.desktop
	echo 'Name=NebiChat' >> $(LINUX_BUILD_DIR)/NebiChat.desktop
	echo 'Version=1.0' >> $(LINUX_BUILD_DIR)/NebiChat.desktop
	echo 'GenericName=Internet Messenger' >> $(LINUX_BUILD_DIR)/NebiChat.desktop
	echo 'Comment=Public & private chat for gamers.' >> $(LINUX_BUILD_DIR)/NebiChat.desktop
	echo 'Categories=Network;' >> $(LINUX_BUILD_DIR)/NebiChat.desktop
	echo 'Exec=./NebiChat' >> $(LINUX_BUILD_DIR)/NebiChat.desktop
	echo 'Icon=nebichat' >> $(LINUX_BUILD_DIR)/NebiChat.desktop
	echo 'Keywords=discord;nebichat;chat;message;messenger;chat;' >> $(LINUX_BUILD_DIR)/NebiChat.desktop
	mkdir -p $(LINUX_BUILD_DIR)/usr/share/icons/hicolor
	for size in 16x16 32x32 48x48 64x64 128x128 256x256 512x512 1024x1024; do \
		mkdir -p $(LINUX_BUILD_DIR)/usr/share/icons/hicolor/$${size}/apps; \
		convert ./icon.png -resize $${size} $(LINUX_BUILD_DIR)/usr/share/icons/hicolor/$${size}/apps/nebichat.png; \
	done
	convert ./icon.png -resize 256x256 $(LINUX_BUILD_DIR)/nebichat.png; \
	wget "https://github.com/AppImage/AppImageKit/releases/download/continuous/$(APPIMAGE_TOOL)"
	chmod a+x $(APPIMAGE_TOOL)
	./$(APPIMAGE_TOOL) $(LINUX_BUILD_DIR) ./build/exe.linux-$(ARCHITECTURE).AppImage
