/*

	NebiStylestrip CSS
	© 2022-2023 NebiSoft. Some Rights Reserved.

*/

/* Set the background color of QDialogs specifically to white */
QDialog, QPrintDialog * {
    background-color: white;
}

/* Set the background color of QMainWindows specifically to white */
QMainWindow {
    background-color: white;
}

a {
	color: --ns-color-blue-normal;
	text-decoration: none;
}

a:hover {
	text-decoration: underline;
}

QPushButton .button-primary {
	border-radius: 8px;
	text-decoration: none;
	font-weight: 700;
	padding: 4px 16px;
}

QPushButton {
	border-radius: 8px;
	text-decoration: none;
	background: white;
	color: #535353;
	border: 1px solid #c5c5c5;
	padding: 4px 16px;
}

QToolButton {
	border-radius: 8px;
	text-decoration: none;
	background: white;
	color: #535353;
	border: 1px solid #c5c5c5;
	padding: 4px 8px;
}

QPushButton:hover, QToolButton:hover {
	border: 1px solid #c5c5c5;
	color: #b5b5b5;
}

QPushButton .button-primary.button-accent {
	background: #6BCDCE;
	color: #f5f5f5;
}

#NebiClient {
	background: #f5f5f5;
	border-radius: 15px;
}

#header {
    border-bottom: 1px solid rgba(0,0,0,0.07);
}

#header QPushButton, #header QToolButton {
    border: 1px solid transparent;
}

#header QPushButton:hover, #header QToolButton:hover {
    border: 1px solid transparent;
    background-color: #f5f5f5;
    color: black;
}

QTextEdit, QLineEdit {
	padding: 4px 8px;
	border-radius: 8px;
	background: white;
	border: 1px double #c5c5c5;

}

QTextEdit:focus, QLineEdit:focus {
	border: 1px solid #6BCDCE;
	border-radius: 8px;
}

QComboBox {
	padding: 4px 8px;
	border-radius: 8px;
	border: 1px solid #c5c5c5;
	outline: none;
}

QComboBox::drop-down {
        border: none;
}

QComboBox:hover {
	border: 1px solid #6BCDCE;
	border-radius: 8px;
}

QTabBar::tab {
    padding: 4px 8px;
    border: none;
    background: #e5e5e5;
    border: 1px solid rgba(0,0,0,0.07);
    border-top: none;
}

QTabBar::tab:hover {
    background: #f5f5f5;
}

QTabBar::tab:selected{
    background: #ffffff;
}

QTabWidget::tab-bar {
    alignment: center;
}

QTabBar::close-button {
    image: url(lib/QtNebiStylestrip/assets/close-symbolic.png);
    subcontrol-position: right;
}

QMenu, QAbstractItemView {
    background: rgba(255, 255, 255, 0.9);
    border: 1px solid rgba(0,0,0,0.07);
    border-radius: 16px;
    padding: 8px;
}

QMenuBar {
    background: #f5f5f5;
    border-bottom: 1px solid rgba(0,0,0,0.07);
}

QMenuBar::item, QMenu::item, QAbstractItemView::item {
    border-radius: 6px;
    padding: 4px 8px;
}

QMenuBar::item:selected, QMenu::item:selected, QAbstractItemView::item:selected {
    background-color: #6BCDCE;
}

QMenu::item::disabled {
    opacity: 0.5;
}

QToolBar#warn-banner {
    background: #ff5f5f;
    color: white;
    padding: 4px 8px;
    border: none;
    border-bottom-left-radius: 14px;
    border-bottom-right-radius: 14px;
}
