import platform, os

def get_platform():
    system = platform.system()
    info = {"pretty_name": f"os.{system.lower()}", "version": platform.version(), "dist": "", "desktop": ""}

    if system == "Windows":
        return info
    elif system == "Darwin":
        info["dist"] = "os.not_linux"
        info["desktop"] = "os.not_linux"
    elif system == "Linux":
        try:
            with open('/etc/os-release', 'r') as file:
                for line in file:
                    if line.startswith('NAME='):
                        info["dist"] = f"os.{line.split('=')[1].strip().lower()[1:-1]}"
                    elif line.startswith('VERSION_ID='):
                        info["version"] = line.split('=')[1].strip().lower()[1:-1]
        except FileNotFoundError:
            info["dist"] = "os.not_linux"
            
        info["desktop"] = os.environ["DESKTOP_SESSION"] if "DESKTOP_SESSION" in os.environ else "os.not_linux"

    return info
