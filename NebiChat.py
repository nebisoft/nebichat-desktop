#!/usr/bin/python3
import sys
import os
import webbrowser
from PySide6.QtCore import *
from PySide6.QtGui import *
from PySide6.QtWebEngineCore import *
from PySide6.QtWebEngineWidgets import *
from PySide6.QtWidgets import *
from sys import platform

if getattr(sys, "frozen", False):
    # The application is frozen
    datadir = os.path.dirname(sys.executable)
else:
    # The application is not frozen
    datadir = os.path.dirname(__file__)

# Now import using the appropriate path
import_path = datadir

if import_path not in sys.path:
    sys.path.append(import_path)

from lib.QtNebiStylestrip.QSS import *
from lib.QtNebiStylestrip.EzFont import *
from lib.QtNebiStylestrip.NebiClient import *

APP_VERSION = "3.0"


class MySplashScreen(QSplashScreen):
    def __init__(self, animation):
        # run event dispatching in another thread
        QSplashScreen.__init__(self, QPixmap())
        self.movie = QMovie(animation)
        self.movie.frameChanged.connect(self.onNextFrame)
        self.movie.start()

    def onNextFrame(self):
        pixmap = self.movie.currentPixmap().scaled(360, 360)
        self.setPixmap(pixmap)
        self.setMask(pixmap.mask())


class WebEnginePage(QWebEnginePage):
    def createWindow(self, _type):
        page = WebEnginePage(self.profile(), self.parent())
        page.urlChanged.connect(self.on_url_changed)
        return page

    @Slot(QUrl)
    def on_url_changed(self, url):
        page = self.sender()
        webbrowser.open(url.toString())
        page.deleteLater()


class MainWindow(NebiClient):
    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)
        self.setWindowIcon(QIcon("ui/img/icon.ico"))
        print(sys.argv)
        if os.path.exists(datadir + "/overlay.py"):
            os.system("python3 " + datadir + "/overlaycheck.py &")
        else:
            print("INFO:", datadir + "/overlay.py", "not exist disabling overlay...")
        self.splash = MySplashScreen("ui/img/loading.gif")
        self.splash.setFixedSize(QSize(360, 360))
        try:
            if sys.argv[1] != "--background":
                self.splash.show()
        except:
            self.splash.show()

        # Initial window size/pos last saved. Use default values for first time

        self.settingsprof = QSettings('NebiSoft', 'NebiChat')
        self.resize(self.settingsprof.value("size", QSize(1150, 600)))
        self.move(self.settingsprof.value("pos", QPoint(50, 50)))
        self.restoreState(self.settingsprof.value("state", self.saveState()))

        self.profile = QWebEngineProfile(f"Default")
        self.profile.setPersistentCookiesPolicy(QWebEngineProfile.PersistentCookiesPolicy.ForcePersistentCookies)
        self.profile.cookieStore().loadAllCookies()

        self.tabs = QTabWidget()
        self.tabs.setStyleSheet("""
                QTabWidget::pane {
            background: transparent;
            border:0;
        }

        QTabBar::tab {
            background: transparent;
        }
                """)
        self.tabs.tabBar().hide()

        self.browser = QWebEngineView()
        QSS.load_stylesheet(self.browser, "lib/QtNebiStylestrip/qss/app.qss")

        self.webpage = WebEnginePage(self.profile, self.browser)
        self.webpage.setBackgroundColor(Qt.GlobalColor.transparent)

        self.browser.setPage(self.webpage)

        self.browser.settings().setAttribute(QWebEngineSettings.FullScreenSupportEnabled, True)
        self.browser.settings().setAttribute(QWebEngineSettings.ErrorPageEnabled, False)
        if sys.argv.__contains__("--gpu-test"):
            self.browser.setUrl(QUrl("chrome://gpu"))
        else:
            self.browser.setUrl(QUrl("http://chat.nebisoftware.com/"))

        # More difficult! We only want to update the url when it's from the
        # correct tab
        self.browser.loadProgress.connect(self.loading)
        self.browser.loadFinished.connect(self.loadFinish)

        # Disabled for bugs -- self.browser.loadFinished.connect(self.init_color_scheme)
        self.browser.page().profile().downloadRequested.connect(self.on_downloadRequested)

        self.menu_bar = None
        self.custom_content = QWidget()
        self.custom_content.setObjectName("NebiClient")
        self.custom_content_layout = QGridLayout()
        self.custom_content.setLayout(self.custom_content_layout)
        self.setCentralWidget(self.custom_content)
        self.custom_content_layout.setContentsMargins(0, 0, 0, 0)
        self.custom_content_layout.setSpacing(0)
        self.custom_content_layout.addWidget(self.browser)
        self.custom_content_layout.addWidget(self.headerBar, 0, 0, Qt.AlignmentFlag.AlignTop)
        self.headerBar.setBackgroundColor("transparent")
        self.custom_content.setSizePolicy(QSizePolicy(QSizePolicy.Policy.Maximum, QSizePolicy.Policy.Maximum))
        self.headerBar.setSizePolicy(QSizePolicy(QSizePolicy.Policy.Minimum, QSizePolicy.Policy.Maximum))
        self.headerBar.parent = self
        self.headerBar.raise_()
        self.headerBar.setMaximumHeight(16)
        self.close_widget = QWidget()
        self.close_layout = QVBoxLayout()
        self.close_layout.setContentsMargins(24, 20, 0, 0)
        self.close_widget.setLayout(self.close_layout)
        self.close_widget.setMaximumWidth(48)
        self.close_layout.addWidget(self.headerBar.btn_close, 0, Qt.AlignmentFlag.AlignTop)
        self.custom_content_layout.addWidget(self.close_widget, 0, 0, Qt.AlignmentFlag.AlignTop)
        self.minimize_widget = QWidget()
        self.minimize_layout = QVBoxLayout()
        self.minimize_layout.setContentsMargins(0, 20, 24, 0)
        self.minimize_widget.setLayout(self.minimize_layout)
        self.minimize_widget.setMaximumWidth(48)
        self.minimize_layout.addWidget(self.headerBar.btn_minimize, 0, Qt.AlignmentFlag.AlignTop)
        self.custom_content_layout.addWidget(self.minimize_widget, 0, 0,
                                             Qt.AlignmentFlag.AlignTop | Qt.AlignmentFlag.AlignRight)
        self.headerBar.setStyleSheet("border-bottom: none;")
        self.setShadowEffect()

        self.browser.settings().setAttribute(QWebEngineSettings.ScreenCaptureEnabled, True)
        self.setCustomShadowEffect(self)

    def setCustomShadowEffect(self, widget):
        shadow = QGraphicsDropShadowEffect()
        shadow.setBlurRadius(8)
        shadow.setColor(QColor(0, 0, 0, 255))
        shadow.setOffset(0, 2)
        widget.setGraphicsEffect(shadow)

    def showApp(self):
        self.show()
        self.activateWindow()

    def closeEvent(self, e):
        # Write window size and position to config file
        self.settingsprof.setValue("size", self.size())
        self.settingsprof.setValue("pos", self.pos())
        self.hide()
        e.ignore

    def on_downloadRequested(self, download):
        old_path = download.downloadFileName()
        suffix = QFileInfo(old_path).suffix()
        path, _ = QFileDialog.getSaveFileName(
            self, "Save File", old_path, "*." + suffix
        )
        if path:
            print("Download to: " + path)
            download.setDownloadDirectory(path.rsplit("/", 1)[0])
            download.setDownloadFileName(path.rsplit("/", 1)[1])
            download.accept()
            download.isFinishedChanged.connect(lambda: self.onDownloadFinished(path.rsplit("/", 1)[1]))

    def onDownloadFinished(self, fn):
        tray.showMessage(fn, "Download finished sucessfully.")

    def loading(self, e):
        print(e)

    def loadFinish(self):
        try:
            if sys.argv.__contains__("--background") != True:
                self.show()
        except:
            self.show()
        self.setWindowTitle(self.browser.page().title())
        self.browser.page().runJavaScript("document.getElementsByTagName('body')[0].classList.add('cl_nebios')")
        self.splash.close()


def quitapp():
    os.system("killall overlay.py")
    os.system("pkill -9 -f overlaycheck.py")
    app.quit()


if platform.__contains__("linux") and sys.argv.__contains__("--wayland") == False:
    if os.environ["XDG_SESSION_TYPE"].lower().__contains__("wayland"):
        print("[NebiChat] INFO: Wayland detected. Disabling due bugs.")
        os.environ["QT_QPA_PLATFORM"] = "xcb"
elif  platform.__contains__("linux") and sys.argv.__contains__("--wayland"):
    print("[NebiChat] WARNING: Wayland support is experimental and won't work on NVIDIA driver.")

app = QApplication(sys.argv)
font = EzFont("lib/QtNebiStylestrip/fonts/Manrope/Manrope-Medium.ttf")
app.setFont(font)
app.setQuitOnLastWindowClosed(False)
app.setApplicationName("NebiChat")

web = MainWindow()

# Adding an icon
icon = QIcon("ui/img/sysTrayIcon.png")

# Adding item on the menu bar
tray = QSystemTrayIcon()
tray.setToolTip("NebiChat")
tray.setIcon(icon)
tray.setVisible(True)

# Creating the options
menu = QMenu()
QSS.load_stylesheet(menu, "lib/QtNebiStylestrip/qss/app.qss")

AppNameItem = QAction("NebiChat Desktop Client")
AppVerItem = QAction("Version " + APP_VERSION)

AppNameItem.setEnabled(False)
AppVerItem.setEnabled(False)
menu.addAction(AppNameItem)
menu.addAction(AppVerItem)
menu.addSeparator()

option1 = QAction("Open NebiChat")
option1.triggered.connect(web.showApp)
menu.addAction(option1)

# To quit the app
quit = QAction("Quit")
quit.triggered.connect(lambda _: quitapp())
menu.addSeparator()
menu.addAction(quit)

# Adding options to the System Tray
tray.setContextMenu(menu)
tray.activated.connect(menu.show)
web.browser.page().setBackgroundColor(Qt.transparent)

if platform == "win32":
    tray.showMessage("Switch to NebiOS",
                     "Switch to NebiOS to get more of NebiChat's features. NebiOS - a universal operating "
                     "system. Faster, better, secure than Windows. Click for more details.", 5000)

app.exec_()
