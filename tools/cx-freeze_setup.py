from cx_Freeze import setup, Executable
buildOptions = dict(packages = [], excludes = [])

import sys
base = 'Win32GUI' if sys.platform =='win32' else None

executables = [
    Executable('NebiChat.py',
    base=base,
    icon = "icon.ico" )
              ]

buildOptions = dict(include_files = ['ui/', 'lib/'])

setup(
    name='NebiChat Desktop',
    version = '2.1',
    description = 'NebiChat Desktop',
    options = dict(build_exe = buildOptions),
    executables = executables
    )